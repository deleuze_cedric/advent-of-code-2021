<?php

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');
echo "<pre>";

$columns = [];
$number = [];

while (!$file->eof()) {
    $line = trim($file->current());

    if (!empty($line)) {
        $numbers[] = $line;
        countNumberInLine($line, $columns);
    }

    $file->next();
}

$o2 = new GeneratorRating($columns, $numbers, true);
$co2 = new GeneratorRating($columns, $numbers, false);

$generators = [
    $o2,
    $co2
];

foreach ($generators as $generator) {
    searchInGeneratorAtPos($generator, 0);
}


$o2Decimal = binaryToDecimal($o2->numbers[0] ?? 0);
$co2Decimal = binaryToDecimal($co2->numbers[0] ?? 0);

$result = $o2Decimal * $co2Decimal;

function countNumberInLine($line, &$columns)
{

    $lineCar = str_split($line);

    foreach ($lineCar as $column => $car) {

        if (!isset($columns[$column])) {
            $columns[$column] = [];
        }

        if (!isset($columns[$column][$car])) {
            $columns[$column][$car] = 0;
        }

        $columns[$column][$car]++;
    }
}

function searchInGeneratorAtPos(GeneratorRating $generator, int $position)
{
    $column = $generator->columns[$position] ?? [];
    $nbZero = $column['0'] ?? 0;
    $nbOne = $column['1'] ?? 0;

    if ($generator->useGreaterCounter) {
        if ($nbOne >= $nbZero) {
            $valSearched = '1';
            $nbSearched = $nbOne;
        } else {
            $valSearched = '0';
            $nbSearched = $nbZero;
        }
    } else {
        if ($nbZero <= $nbOne) {
            $valSearched = '0';
            $nbSearched = $nbZero;
        } else {
            $valSearched = '1';
            $nbSearched = $nbOne;
        }
    }

    $generator->numbers = searchXFirstNumberWithPositionYIsValueZInNumbers($nbSearched, $position, $valSearched, $generator->numbers);

    if (count($generator->numbers) > 1) {

        $generator->columns = [];

        foreach ($generator->numbers as $number) {
            countNumberInLine($number, $generator->columns);
        }

        searchInGeneratorAtPos($generator, $position + 1);
    }
}

function searchXFirstNumberWithPositionYIsValueZInNumbers($nbSearched, $position, $searchedValue, $numbers)
{

    $res = [];

    foreach ($numbers as $number) {
        $numberArray = str_split($number);

        if (($numberArray[$position] ?? -1) == $searchedValue) {
            $res[] = $number;

            if (count($res) >= $nbSearched) {
                break;
            }
        }
    }


    return $res;
}

function binaryToDecimal(string $binary): int
{
    $numbers = str_split($binary);
    $numbers = array_reverse($numbers);

    $decimal = 0;

    foreach ($numbers as $power => $bit) {
        $decimal +=  pow(2, $power) * (int)$bit;
    }

    return $decimal;
}

class GeneratorRating
{

    public function __construct(
        public array $columns,
        public array $numbers,
        public bool $useGreaterCounter
    ) {
    }
}
