<?php

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');
echo "<pre>";

$columns = [];

while (!$file->eof()) {
    $line = $file->current();

    if (!empty($line)) {
        $lineCar = str_split(trim($line));

        foreach ($lineCar as $column => $car) {


            if (!isset($columns[$column])) {
                $columns[$column] = [];
            }

            if (!isset($columns[$column][$car])) {
                $columns[$column][$car] = 0;
            }

            $columns[$column][$car]++;
        }
    }

    $file->next();
}

$gamma = '';
$epsilon = '';

foreach ($columns as $column) {
    $nbZero = $column['0'] ?? 0;
    $nbOne = $column['1'] ?? 0;
    $gamma .= ($nbZero < $nbOne ? '1' : '0');
    $epsilon .= ($nbZero > $nbOne ? '1' : '0');
}

$gammaDecimal = binaryToDecimal($gamma);
$epsilonDecimal = binaryToDecimal($epsilon);

$power = $gammaDecimal * $epsilonDecimal;
var_dump($power);

function binaryToDecimal(string $binary): int
{
    $numbers = str_split($binary);
    $numbers = array_reverse($numbers);

    $decimal = 0;

    foreach ($numbers as $power => $bit) {
        $decimal +=  pow(2, $power) * (int)$bit;
    }

    return $decimal;
}
