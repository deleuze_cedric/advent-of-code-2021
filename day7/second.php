<?php

require_once __DIR__ . '/data.php';
//$data = [2, 7, 3, 12, 9];
if (!empty($data)) {

    $totalFuel = 0;
    echo "<pre>";
    sort($data);

    $firstData = $data[0] ?? 0;

//    On prend le 1er chiffre et on calcule le fuel total nécessaire pour que tous les crabes se mettent sur cette
//    position. On utilisera cette valeur comme origine pour le reste du programme.
    foreach ($data as $number) {
//        On récupère la différence entre le 1er chiffre et celui actuel
        $diff = abs($firstData - $number);

//        On incrémente d'autant de chiffre que la différence est grande (9 - 6 = 5 => on fera donc + 1 + 2 + 3 + 4 + 5)
        for ($i = 1; $i <= $diff; $i++) {
            $totalFuel += $i;
        }
    }

//    var_dump($data);
//    var_dump($totalFuel);

    $lastData = $firstData;
    $lastFuel = null;

//    Pour chaque chiffre entre 1 et le dernier nombre
    for ($i = 1; $i < $data[count($data) - 1]; $i++) {

//        var_dump("Valeur : " . $i);

//        On décrémente de la valeur de la différence entre le chiffre actuel et les chiffres supérieurs
//        et on incrémente de la valeur de la différence entre le chiffre actuel et les chiffres inférieurs
//        Ainsi dans un tableau [0, 2, 4, 6]
//        Si l'on a comme chiffre actuel 5. Le précédent était 4.
//        Nous faisaons donc + (5 - 0) + (5 - 2) + (5 - 4) + (5 - 6) => Ce qui nous donne un total de +8
        for ($j = 0; $j < count($data); $j++) {
            $diff = ($i + ($data[$j] >= $i ? -1 : 0)) - $data[$j];
            $totalFuel += $diff;
//            var_dump("J : " . $data[$j] . " => " . $diff);
        }

//        var_dump($totalFuel);

//        On arrête la boucle dès que le total de fuel précédent est inférieur à celui en cours
        if (!is_null($lastFuel)) {
            if ($lastFuel < $totalFuel) {
//                var_dump($i);
                $totalFuel = $lastFuel;
                break;
            } else {
                $lastFuel = $totalFuel;
            }
        } else {
            $lastFuel = $totalFuel;
        }
    }

    echo $totalFuel . "<br/>";
}

