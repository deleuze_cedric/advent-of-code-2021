<?php

use JetBrains\PhpStorm\Pure;

#[Pure] function getCharacterObject(string $charSearch): ?Character
{

    $characters = [
        new Character(
            '(',
            ')',
            3,
            1
        ),
        new Character(
            '[',
            ']',
            57,
            2
        ),
        new Character(
            '{',
            '}',
            1197,
            3
        ),
        new Character(
            '<',
            '>',
            25137,
            4
        ),
    ];

    foreach ($characters as $character) {
        if ($character->openChar == $charSearch || $character->closeChar == $charSearch) {
            return $character;
        }
    }

    return null;

}

class Line
{

    protected array $characters = [];

    /**
     * @var array|Character[]
     */
    protected array $illegalCharacters = [];

    /**
     * @var array|Character[]
     */
    protected array $closedChar = [];

    public function __construct(
        public string $line
    )
    {
        $this->characters = str_split($this->line);
    }

    public function detectIllegalCharactersOrNotEndedLine(int $nbIllegalData = 1)
    {

        $openTagsNotClosed = [];

//        var_dump($this->line);

        foreach ($this->characters as $character) {
            $charObject = getCharacterObject($character);

            if (!empty($charObject)) {

//                echo "<pre>";
//                var_dump($character, $charObject->isOpenChar($character),  $charObject->isCloseChar($character));
//                echo "</pre>";

                if ($charObject->isOpenChar($character)) {
                    $openTagsNotClosed[] = $character;
                } else if ($charObject->isCloseChar($character)) {

//                    On récupère l'index dans le tableau des tags ouverts
//                    Taille du tableau 'open Tags' - Taille du tableau 'close Tags'
                    $testIndexOpenTags = array_key_last($openTagsNotClosed);
//                    var_dump($testIndexOpenTags, $openTagsNotClosed);

                    if (isset($openTagsNotClosed[$testIndexOpenTags])) {

//                        echo "<pre>";
//                        var_dump($openTagsNotClosed[$testIndexOpenTags]);
//                        echo "</pre>";

                        if (!$charObject->isOpenChar($openTagsNotClosed[$testIndexOpenTags])) {
                            $this->illegalCharacters[] = $charObject;

                            if (count($this->illegalCharacters) >= $nbIllegalData) {
                                break;
                            }
                        } else {
                            unset($openTagsNotClosed[$testIndexOpenTags]);
                        }

                    }
                }
//                echo "<pre>";
//                var_dump($openTagsNotClosed, '--------------------');
//                echo "</pre>";

            }
        }

        if (empty($this->illegalCharacters)) {

//            echo "<pre>";
//            var_dump($openTagsNotClosed);
//            echo "</pre>";

            foreach ($openTagsNotClosed as $openTags) {

                $charObject = getCharacterObject($openTags);

                if (!empty($charObject)) {
                    $this->closedChar[] = $charObject;
                }

            }

            $this->closedChar = array_reverse($this->closedChar);
        }
    }

    public function getIllegalCharactersPoints(): int
    {
        $points = 0;

        foreach ($this->illegalCharacters as $illegalCharacter) {
            $points += $illegalCharacter->illegalPoint;
        }

        return $points;
    }

    public function getCloseCharactersPoints(): int
    {
        $points = 0;

        foreach ($this->closedChar as $closedChar) {
            $points = ($points * 5) + $closedChar->closePoint;
        }

        return $points;
    }

}

class Character
{
    public function __construct(
        public string $openChar,
        public string $closeChar,
        public int $illegalPoint,
        public int $closePoint
    )
    {
    }

    public function isOpenChar(string $char): bool
    {
        return $this->openChar == $char;
    }

    public function isCloseChar(string $char): bool
    {
        return $this->closeChar == $char;
    }
}

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');
$lines = [];
$totalPointsIllegal = 0;
$totalPointsClose = 0;
$pointsLinesClosed = [];

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {
        $lineObj = new Line($line);
        $lineObj->detectIllegalCharactersOrNotEndedLine();
        $totalPointsIllegal += $lineObj->getIllegalCharactersPoints();
        $pointsClose = $lineObj->getCloseCharactersPoints();

        if($pointsClose){
            $pointsLinesClosed[] = $pointsClose;
        }

        $lines[] = $lineObj;
    }

    $file->next();
}

if(count($pointsLinesClosed) %2 === 1){
    sort($pointsLinesClosed);
    $totalPointsClose = $pointsLinesClosed[((count($pointsLinesClosed)-1)/2)];
}
