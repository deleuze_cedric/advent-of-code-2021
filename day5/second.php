<?php

class Board
{

    public array $lines = [];

    public function __toString()
    {
        $html = '';

        foreach ($this->lines as $line) {
            $html .= $line;
        }

        return "<table>" . $html . "</table>";
    }

    public function countNbPointHavingMore2Intersection(): CounterLineNumber
    {
        $counter = new CounterLineNumber();

        foreach ($this->lines as $line) {
            $counter->counter += $line->countNbPointHavingMore2Intersection()->counter;
        }

        return $counter;
    }

    public function growToLength($vector)
    {
        $maxX = $vector->getMaxX();
        $maxY = $vector->getMaxY();

        while (count($this->lines) <= $maxY) {
            $this->lines[] = new Line();
        }

        foreach ($this->lines as $line) {
            $line->growToWidth($maxX);
        }
    }

    public function incrementPoints(Vector $vector)
    {
        $this->growToLength($vector);

        $minX = $vector->getMinX();
        $maxX = $vector->getMaxX();
        $minY = $vector->getMinY();
        $maxY = $vector->getMaxY();

        if ($vector->xEquals()) {
            // x1 = x2
            $subLines = array_slice($this->lines, $minY, $maxY - $minY + 1);

            foreach ($subLines as $line) {
                $line->incrementPoint($vector->x1);
            }
        } else if ($vector->yEquals() && !empty($this->lines[$vector->y1])) {
            // y1 = y2
            $this->lines[$vector->y1]->incrementsPointsFromTo($minX, $maxX);
        } else 
        if ($vector->is45DegreeDiagonal()) {
            // abs(x1 - x2) = abs (y1 - y2)

            for ($i = 0; $i <= abs($vector->getXDiff()); $i++) {

                $yPos = $vector->y1 + ($vector->getYDiff() < 0 ? $i : -$i);
                $xPos = $vector->x1 + ($vector->getXDiff() < 0 ? $i : -$i);

                // echo "<pre>";
                // var_dump(
                //     $vector->x1,
                //     $vector->y1,
                //     $vector->x2,
                //     $vector->y2,
                //     $vector->getYDiff(),
                //     $vector->getXDiff(),
                //     $xPos,
                //     $yPos
                // );
                // echo "</pre>";

                $this->lines[$yPos]->incrementPoint($xPos);
            }
        }
    }
}

class Line
{
    public array $points = [];

    public function growToWidth($width)
    {
        while (count($this->points) <= $width) {
            $this->points[] = new Point();
        }
    }

    public function incrementPoint($columnPos)
    {
        if (!empty($this->points[$columnPos])) {
            $this->points[$columnPos]->increment();
        }
    }

    public function incrementsPointsFromTo($minX, $maxX)
    {

        $subPoints = array_slice($this->points, $minX, $maxX - $minX + 1);

        foreach ($subPoints as $point) {
            $point->increment();
        }
    }

    public function __toString()
    {
        $str = '';

        foreach ($this->points as $point) {
            $str .= $point;
        }

        return '<tr>' . $str . '</tr>';
    }

    public function countNbPointHavingMore2Intersection(): CounterLineNumber
    {
        $counter = new CounterLineNumber();

        foreach ($this->points as $point) {
            if ($point->vectorIntersectionNumber >= 2) {
                $counter->counter++;
            }
        }

        return $counter;
    }
}

class Point
{
    public function __construct(
        public int $vectorIntersectionNumber = 0
    ) {
    }

    public function increment()
    {
        $this->vectorIntersectionNumber++;
    }

    public function __toString()
    {
        $str = '.';

        if ($this->vectorIntersectionNumber > 0) {
            $str = (string)$this->vectorIntersectionNumber;
        }

        return '<td>' . $str . '</td>';
    }
}

class CounterLineNumber
{
    public int $counter = 0;
}

class Vector
{
    public function __construct(
        public int $x1 = 0,
        public int $y1 = 0,
        public int $x2 = 0,
        public int $y2 = 0
    ) {
    }

    public function getMaxX()
    {
        return max($this->x1, $this->x2);
    }

    public function getMaxY()
    {
        return max($this->y1, $this->y2);
    }

    public function getMinX()
    {
        return min($this->x1, $this->x2);
    }

    public function getMinY()
    {
        return min($this->y1, $this->y2);
    }

    public function xEquals()
    {
        return $this->x1 == $this->x2;
    }

    public function yEquals()
    {
        return $this->y1 == $this->y2;
    }

    public function is45DegreeDiagonal()
    {
        return abs($this->getXDiff()) == abs($this->getYDiff());
    }

    public function getXDiff()
    {
        return $this->x1 - $this->x2;
    }

    public function getYDiff()
    {
        return $this->y1 - $this->y2;
    }
}

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');

$board = new Board();

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {

        $positions = preg_split('/( -> )/', $line);
        // var_dump(preg_split('/(\d+?)(?= *)/', $line));

        $boardLine = new Line();

        $coordinates = [];

        foreach ($positions as $position) {
            $coordinates[] = explode(',', $position);
        }

        // Si x1 = x2
        // ou y1 = y2
        if (
            isset($coordinates[0][0]) &&
            isset($coordinates[0][1]) &&
            isset($coordinates[1][0]) &&
            isset($coordinates[1][1])
        ) {
            $vector = new Vector(
                $coordinates[0][0],
                $coordinates[0][1],
                $coordinates[1][0],
                $coordinates[1][1],
            );

            // echo "<pre>";
            // print_r($coordinates);
            // print_r($vector);
            // echo "</pre>";
            $board->incrementPoints($vector);
        }

        $file->next();
    }
}

$lineCounter = $board->countNbPointHavingMore2Intersection();

// echo $board . "<br/>";
echo $lineCounter->counter . "<br/>";
