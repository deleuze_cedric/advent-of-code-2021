<?php

use JetBrains\PhpStorm\Pure;

class Map
{

    /**
     * @var Point[][] $points
     */
    public array $points = [];

    /**
     * @var Basin[] $basins
     */
    public array $basins = [];

    public function __toString(): string
    {
        $html = "<table>";

        foreach ($this->points as $row => $points) {

            $html .= "<tr>";

            foreach ($points as $col => $point) {
                $html .= "<td>" . ($point->isLowestPoint ? "<b style='color: blue';>" : "") . $point . ($point->isLowestPoint ? "</b>" : "") . "</td>";
            }

            $html .= "</tr>";
        }

        $html .= "</table>";

        return $html;
    }

    #[Pure] public function getAdjacentPointsOfPoint(Point $point): array
    {
        $yAdjacents = $point->getYAdjacents();
        $xAdjacents = $point->getXAdjacents();

        $adjacentsPoints = [];

        foreach ($this->points as $row => $points) {
            foreach ($points as $col => $pointToCompare) {

                if (
                    !($point->y == $pointToCompare->y && $point->x == $pointToCompare->x) &&
                    in_array($pointToCompare->y, $yAdjacents) &&
                    in_array($pointToCompare->x, $xAdjacents)
                ) {
                    $adjacentsPoints[] = $pointToCompare;
                }

            }
        }

        return $adjacentsPoints;

    }

    public function defineLowestPoints()
    {

        foreach ($this->points as $row => $points) {
            foreach ($points as $point) {
                $point->isLowestPoint = $point->isLowerThanOtherPoints($this->getAdjacentPointsOfPoint($point));
            }
        }

    }

    public function getLowerPoints(): array
    {

        $lowerPoints = [];

        foreach ($this->points as $row => $points) {
            foreach ($points as $col => $point) {
                if ($point->isLowestPoint) {
                    $lowerPoints[] = $point;
                }
            }
        }

        return $lowerPoints;

    }

    public function sumLowestPoints(): int
    {
        $total = 0;

        foreach ($this->getLowerPoints() as $point) {
            $total += $point->value + 1;
        }

        return $total;
    }

    public function getWidth(): int
    {
        $width = 0;

        foreach ($this->points as $row => $points) {
            if (count($points) > $width) {
                $width = count($points);
            }
        }

        return $width;

    }

    public function getHeight(): int
    {
        return count($this->points);
    }

    public function determineBasins()
    {

        foreach ($this->getLowerPoints() as $point) {
            $basin = new Basin($this);
            $basin->expandFromPoint($point);
            $this->basins[] = $basin;
        }

    }

    public function toStringBasins(): string
    {

        $html = "";

        foreach ($this->basins as $basin) {
            $html .= $basin . "<br/>";
        }

        return $html;

    }

    public function sumBasinsSize($nbBestBasin = 3): int
    {

        $total = 1;

        $bestBasins = [];

        $basinsCount = [];

        foreach ($this->basins as $basin) {
            $basinsCount[] = count($basin->points, COUNT_RECURSIVE) - count($basin->points);
        }

        sort($basinsCount);

        $nb = 0;

        foreach (array_reverse($basinsCount) as $bestBasin) {
            $total *= $bestBasin;
            $nb++;

            if($nb >= 3){
                break;
            }
        }

        return $total;

    }

}

class Point
{

    public function __construct(
        public int $value,
        public int $x,
        public int $y,
        public bool $isLowestPoint = false
    )
    {
    }

    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @param Point[] $points
     * @return bool
     */
    public function isLowerThanOtherPoints(array $points): bool
    {

        $lower = true;

        foreach ($points as $point) {
            $lower &= $this->value < $point->value;
        }

        return $lower;

    }

    public function getYAdjacents(): array
    {

        return [
            $this->y - 1,
            $this->y,
            $this->y + 1,
        ];

    }

    public function getXAdjacents(): array
    {
        return [
            $this->x - 1,
            $this->x,
            $this->x + 1,
        ];
    }


}

class Basin
{

    /**
     * @var array[]Point[] $points
     */
    public array $points = [];

    public function __construct(
        public Map $map
    )
    {
    }

    public function expandFromPoint(Point $initialPoint)
    {
        if ($initialPoint->value != 9) {

            $yAdjacents = $initialPoint->getYAdjacents();
            $xAdjacents = $initialPoint->getXAdjacents();

            foreach ($yAdjacents as $indexYPos => $yPos) {

                if ($yPos >= 0 && $yPos < $this->map->getHeight()) {

                    foreach ($xAdjacents as $indexXPos => $xPos) {

                        if (
                            ($indexYPos == 1 || $indexXPos == 1) &&
                            $xPos >= 0 &&
                            $xPos < $this->map->getWidth()
                        ) {

                            $point = $this->map->points[$yPos][$xPos];

                            if ($point->value != 9 && !isset($this->points[$yPos][$xPos])) {
                                $this->points[$yPos][$xPos] = $point;
                                $this->expandFromPoint($point);
                            }
                        }

                    }
                }
            }
        }

    }

    public function __toString(): string
    {

        $html = "<table>";

        foreach ($this->map->points as $row => $points) {

            $html .= "<tr>";

            foreach ($points as $col => $point) {
                $pointInBasin = $this->points[$row][$col] ?? false;
                $html .= "<td>" . ($pointInBasin ? "<b>" : "") . $point . ($pointInBasin ? "</b>" : "") . "</td>";
            }

            $html .= "</tr>";
        }

        $html .= "</table>";

        return $html;
    }

}

$map = new Map();
$file = new SplFileObject(__DIR__ . '/data.txt', 'r');

$row = 0;

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {

        $data = str_split($line);
        foreach ($data as $index => $value) {
            $map->points[$row][$index] = new Point((int)$value, $index, $row);
        }
    }

    $row++;
    $file->next();
}

$map->defineLowestPoints();
