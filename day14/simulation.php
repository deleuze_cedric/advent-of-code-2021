<?php

use JetBrains\PhpStorm\Pure;

class Instruction
{

    public function __construct(
        public string $firstLetter,
        public string $secondLetter,
        public string $newLetter,
        public int $nbOccurence,
    )
    {
    }

    public function getLetters(): string
    {
        return $this->firstLetter . $this->secondLetter;
    }

    #[Pure] public function __toString(): string
    {
        return $this->getLetters() . ' => ' . $this->nbOccurence;
    }

}

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');
$template = '';
$instructions = [];
$isInstructionPart = false;

echo "<pre>";

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {

        if ($isInstructionPart) {
            $instructionsInfo = explode(' -> ', $line);
            $enterLetters = $instructionsInfo[0];
            $letters = str_split($enterLetters);
            $newLetter = $instructionsInfo[1];
            $pattern = new Instruction($letters[0], $letters[1], $newLetter, 0);
            $instructions[$enterLetters] = $pattern;
        } else {
            $template = $line;
        }

    } else {
        $isInstructionPart = true;
    }

    $file->next();
}

$nbLetterTemplate = strlen($template);

for ($i = 0; $i < $nbLetterTemplate; $i++) {

    if ($i + 1 < $nbLetterTemplate) {
        $currentLetter = $template[$i];
        $nextLetter = $template[$i + 1];
        $letters = $currentLetter . $nextLetter;
        $instructions[$letters]->nbOccurence++;
    }

}

//printInstructions($instructions);

function doSimultation(int $nbStepToDo, array $instructions, string $originalTemplate)
{
//    echo 'Template : ' . $template . "<br/>";

    for ($step = 0; $step < $nbStepToDo; $step++) {

        $newIntructions = [];

        foreach ($instructions as $instruction) {
            if ($instruction->nbOccurence > 0) {
                $firstPackageLetter = $instruction->firstLetter . $instruction->newLetter;
                $secondPackageLetter = $instruction->newLetter . $instruction->secondLetter;

                if (!isset($newIntructions[$firstPackageLetter])) {
                    $newIntructions[$firstPackageLetter] = 0;
                }

                if (!isset($newIntructions[$secondPackageLetter])) {
                    $newIntructions[$secondPackageLetter] = 0;
                }

//                var_dump($firstPackageLetter, $newIntructions[$firstPackageLetter], $secondPackageLetter, $newIntructions[$secondPackageLetter], $instruction->nbOccurence, '____');
                $newIntructions[$firstPackageLetter] += $instruction->nbOccurence;
                $newIntructions[$secondPackageLetter] += $instruction->nbOccurence;
            }
        }


        foreach ($instructions as $letters => $instruction) {
            $instruction->nbOccurence = $newIntructions[$letters] ?? 0;
        }

        echo "<br/>Etape " . ($step + 1) . ' :<br/>';
        printInstructions($instructions);
    }

    printMaxMinValueFromInstructions($instructions, $originalTemplate);
}

function printMaxMinValueFromInstructions(array $instructions, string $originalTemplate)
{

    $nbLettersTotal = [];

//    On ne compte que les premières lettres (pour éviter les doublons).
//    On ajoute par la suite le dernier carac oublié
    foreach ($instructions as $instruction) {

        if (!isset($nbLettersTotal[$instruction->firstLetter])) {
            $nbLettersTotal[$instruction->firstLetter] = 0;
        }

        $nbLettersTotal[$instruction->firstLetter] += $instruction->nbOccurence;
    }

    $lastLetter = $originalTemplate[strlen($originalTemplate) - 1];
    $nbLettersTotal[$lastLetter]++;

    $minLetter = '';
    $minValue = null;
    $maxLetter = '';
    $maxValue = null;

    foreach ($nbLettersTotal as $letter => $val) {
        if ($val < $minValue || is_null($minValue)) {
            $minValue = $val;
            $minLetter = $letter;
        }
        if ($val > $maxValue || is_null($maxValue)) {
            $maxValue = $val;
            $maxLetter = $letter;
        }
    }

    echo "Letter avec le moins d'occurences : " . $minLetter . ' => ' . $minValue . "<br/>";
    echo "Letter avec le plus d'occurences : " . $maxLetter . ' => ' . $maxValue . '<br/>';
    echo "Différence : " . ($maxValue - $minValue);

}

function printInstructions(array $instructions)
{
    $nbTotalOccurence = 0;

    foreach ($instructions as $instruction) {
        if ($instruction->nbOccurence > 0) {
            echo $instruction . "<br/>";
            $nbTotalOccurence += $instruction->nbOccurence;
        }
    }

    echo "Total occurences : " . $nbTotalOccurence . "<br/><br/>";
}