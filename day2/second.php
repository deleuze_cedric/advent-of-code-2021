<?php

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');

$horizontal = 0;
$depth = 0;
$aim = 0;

while (!$file->eof()) {
    $line = $file->current();

    $data = explode(' ', $line);
    $instruction = $data[0] ?? null;
    $amount = $data[1] ?? 0;
    
    if (!empty($instruction) && !empty($amount)) {
        $amount = (int)$amount;
        switch ($instruction) {
            case 'forward':
                $horizontal += $amount;
                $depth += $aim * $amount;
                break;
            case 'down':
                $aim += $amount;
                break;
            case 'up':
                $aim -= $amount;
                break;
        }
    }


    $file->next();
}

$result = $horizontal * $depth;
echo $result;
