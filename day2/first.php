<?php

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');

$horizontal = 0;
$depth = 0;

while (!$file->eof()) {
    $line = $file->current();

    $data = explode(' ', $line);
    $instruction = $data[0] ?? null;
    $amount = $data[1] ?? 0;
    $amount = (int)$amount;
    
    if (!empty($instruction) && !empty($amount)) {
        switch ($instruction) {
            case 'forward':
                $horizontal += $amount;
                break;
            case 'down':
                $depth += $amount;
                break;
            case 'up':
                $depth -= $amount;
                break;
        }
    }


    $file->next();
}

$result = $horizontal * $depth;
echo $result;
