<?php

//        000
//       1   6
//       1   6
//       1   6
//        222
//       3   5
//       3   5
//       3   5
//        444
use JetBrains\PhpStorm\NoReturn;
use JetBrains\PhpStorm\Pure;

class Number
{

    public function __construct(
        public int $number,
        public array $position = [],
        public bool $useUniqueFormatDigit = false,
    )
    {
    }

    public function getLengthPosition(): int
    {
        return count($this->position);
    }

}


$numbers = [
    0 => new Number(0, [0, 1, 3, 4, 5, 6],),
    1 => new Number(1, [5, 6], true),
    2 => new Number(2, [0, 2, 3, 4, 6]),
    3 => new Number(3, [0, 2, 4, 5, 6]),
    4 => new Number(4, [1, 2, 5, 6], true),
    5 => new Number(5, [0, 1, 2, 4, 5]),
    6 => new Number(6, [0, 1, 2, 3, 4, 5]),
    7 => new Number(7, [0, 5, 6], true),
    8 => new Number(8, [0, 1, 2, 3, 4, 5, 6], true),
    9 => new Number(9, [0, 1, 2, 3, 4, 5]),
];

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');
echo "<pre>";

$total = 0;

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {

        $data = explode(' | ', $line);
        $inputsTxt = $data[0];
        $outputsTxt = $data[1];

        $inputs = explode(' ', $inputsTxt);
        $outputs = explode(' ', $outputsTxt);
        $total += decodeLine($inputs, $outputs, $numbers);

    }

    $file->next();
}

var_dump($total);

#[NoReturn] function decodeLine(array $inputs, array $outputs, array $numbers): int
{

    $totalLine = '';

    $allData = array_merge($inputs, $outputs);

    $lettersToNumberFound = [];
    $lettersToNumber = [];

    foreach ($allData as $data) {

        $data = sortString($data);

        /**
         * @var Digit $uniqueDigit
         */
        foreach ($numbers as $number) {

//            On classe d'abord les chiffres avec un code unique
            if (!isset($lettersToNumber[$data])) {
                if ($number->useUniqueFormatDigit) {
                    if ($number->getLengthPosition() == strlen($data)) {
                        $lettersToNumberFound[$number->number] = $data;
                        break;
                    }
                } else {
                    $lettersToNumber[$data] = null;
                }
            }

        }

    }

    $one = $lettersToNumberFound[1];
    $oneArray = str_split($one);

    $four = $lettersToNumberFound[4];
    $fourArray = str_split($four);

    do {

        foreach ($lettersToNumber as $letters => &$number) {

            if (is_null($number)) {

                $lettersArray = str_split($letters);

                switch (strlen($letters)) {
                    case 5:

//                        Si le chiffre est des digits de 1 => 3
                        if (!empty(numberIsComposedOfLetterOfAnotherNumber($lettersArray, $oneArray))) {
                            $lettersToNumberFound[3] = $letters;
                        } else {
//                         Sinon si le chiffre est composé de 3 digits des 4 du chiffre 4 => 5
                            if (countSameLetterOfTwoNumber($lettersArray, $fourArray) == 3) {
                                $lettersToNumberFound[5] = $letters;
                            } else {
//                                Sionon c'est 2 (2 digits du chiffre 4)
                                $lettersToNumberFound[2] = $letters;
                            }
                        }

                        break;
                    case 6:


//         Si le chiffre est composé des digits de 1 => O ou 9
                        if (numberIsComposedOfLetterOfAnotherNumber($lettersArray, $oneArray)) {

//             Si le chiffre est composé des digits de 4 => 9
                            if (numberIsComposedOfLetterOfAnotherNumber($lettersArray, $fourArray)) {
                                $lettersToNumberFound[9] = $letters;
                            } else {
//                                Sinon 0
                                $lettersToNumberFound[0] = $letters;
                            }
//                            Sinon 6
                        } else {
                            $lettersToNumberFound[6] = $letters;
                        }


                        break;
                }
            }
        }

    } while (count($lettersToNumberFound) != count($numbers));

    foreach ($outputs as $data) {
        $number = array_search(sortString($data), $lettersToNumberFound);
        $totalLine .= $number;
    }

    return (int)$totalLine;
}

function sortString(string $string): string
{
    $dataArray = str_split($string);
    sort($dataArray);
    return implode('', $dataArray);
}

function numberIsComposedOfLetterOfAnotherNumber(array $lettersNumberOne, array $lettersToOwn): bool
{

    $haveAll = true;

    foreach ($lettersToOwn as $letterToOwn) {

        $find = false;

        foreach ($lettersNumberOne as $letter) {

            if ($letter == $letterToOwn) {
                $find = true;
                break;
            }

        }

        $haveAll &= $find;

    }

    return $haveAll;
}

function countSameLetterOfTwoNumber(array $lettersNumberOne, array $lettersNumberTwo): int
{

    $nbSame = 0;

    foreach ($lettersNumberOne as $letterN1) {

        foreach ($lettersNumberTwo as $letterN2) {

            if ($letterN1 == $letterN2) {
                $nbSame++;
                break;
            }

        }

    }

    return $nbSame;

}

