<?php

class Digit
{

    public function __construct(
        public int $number,
        public int $nbDigit,
        public int $nbOccurences = 0
    )
    {
    }

}

$digits = [
    1 => new Digit(1, 2),
    2 => new Digit(2, 5),
    3 => new Digit(3, 5),
    4 => new Digit(4, 4),
    5 => new Digit(5, 5),
    6 => new Digit(6, 6),
    7 => new Digit(7, 3),
    8 => new Digit(8, 7),
    9 => new Digit(9, 6),
];

$uniqueDigits = [];

foreach ($digits as $digit) {

    $sameNbDigit = false;

    foreach ($digits as $digitCompare) {
        if ($digit->number !== $digitCompare->number && $digitCompare->nbDigit == $digit->nbDigit) {
            $sameNbDigit = true;
            break;
        }
    }

    if (!$sameNbDigit) {
        $uniqueDigits[] = $digit;
    }
}


$file = new SplFileObject(__DIR__ . '/data.txt', 'r');

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {

        $data = explode(' | ', $line);
        $inputsTxt = $data[0];
        $outputsTxt = $data[1];

        $outputs = explode(' ', $outputsTxt);

        foreach ($outputs as $output) {

            /**
             * @var Digit $uniqueDigit
             */
            foreach ($uniqueDigits as $uniqueDigit) {
                if ($uniqueDigit->nbDigit == strlen($output)) {
                    $uniqueDigit->nbOccurences++;
                    break;
                }
            }
        }


    }

    $file->next();
}

echo "<pre>";
var_dump($uniqueDigits);

$nbOccurence = 0;

foreach ($uniqueDigits as $uniqueDigit) {
    $nbOccurence += $uniqueDigit->nbOccurences;
}

var_dump($nbOccurence);

