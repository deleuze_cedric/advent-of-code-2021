<?php

const BIRTH_DAY_BEFORE_NEW_BORN = 8;
const NEW_CYCLE_DAY_BEFORE_NEW_BORN = 6;

if (!empty($data) && !empty($nbDayToPredict)) {

    $livesCycle = [];

    for ($i = 0; $i <= BIRTH_DAY_BEFORE_NEW_BORN; $i++) {
        $livesCycle[$i] = count(array_keys($data, $i));
    }

//    echo "<pre>";
//    print_r($data);
//    print_r($livesCycle);

    for ($i = 0; $i < $nbDayToPredict; $i++) {

        $newLanternfish = 0;
        $lanternfishDelivered = 0;
        $newLivesCycle = $livesCycle;

        foreach ($newLivesCycle as $lifeCycle => $nbLanternFish){
            $newLivesCycle[$lifeCycle] = 0;
        }

        foreach ($livesCycle as $lifeCycle => $nbLanternFish) {
            if ($lifeCycle == 0) {
                $newLivesCycle[NEW_CYCLE_DAY_BEFORE_NEW_BORN] += $nbLanternFish;
                $newLivesCycle[BIRTH_DAY_BEFORE_NEW_BORN] += $nbLanternFish;
            } else {
                $newLivesCycle[$lifeCycle - 1] += $nbLanternFish;
            }
        }

        $livesCycle = $newLivesCycle;

//        print_r($livesCycle);

    }

//    echo "</pre>";
    $nbLanternFish = 0;

    foreach ($livesCycle as $lifeCycle) {
        $nbLanternFish += $lifeCycle;
    }

    echo "NB Lanterfish : " . $nbLanternFish;

}