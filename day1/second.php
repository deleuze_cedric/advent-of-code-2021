<?php

require_once(__DIR__ . '/data.php');

$nbIncrease = 0;

$nbLetterInSameTime = 3;

if ($nbLetterInSameTime < 3) {
    throw new Exception("Il doit y avoir au moins 3 lettres");
}

if ($nbLetterInSameTime > 26) {
    throw new Exception("Il doit y avoir moins de 27 lettres");
}

$firstLetter = 'a';
$lettersValue = [
    $firstLetter => new Letter($firstLetter, 0, 1)
];

echo "<pre>";

// Pour chaque nombre
foreach ($data as $number) {

    $unsetLetterIndex = null;
    $addLetter = null;

    // Pour chaque lettre dans le tableau
    foreach ($lettersValue as $indexLetter => $letter) {

        // Si le nombre de fois on l'on est passé sur la variable est < au nombre de lettre à traiter simultannément
        if ($letter->nbIncrement <= $nbLetterInSameTime) {

            // On additionne le nombre avec la valeur
            $letter->value += $number;

            switch ($letter->nbIncrement) {
                // Si c'est le deuxième tour on ajoute une lettre
                case 2:
                    $addLetter = new Letter(
                        $letter->getNextLetter(),
                        $number
                    );
                    break;
                    // Si c'est le dernier tour et que la lettre précédente existe on vérifie si les valeurs sont cohérentes
                case $nbLetterInSameTime:
                    $prevLetter = $letter->getPrevLetter();
                    if (isset($lettersValue[$prevLetter])) {
                        if ($lettersValue[$prevLetter]->value < $letter->value) {
                            $nbIncrease++;
                        }
                        $unsetLetterIndex = $prevLetter;
                        var_dump($lettersValue[$prevLetter]->letter . ' : ' . $lettersValue[$prevLetter]->value . ' => ' . $letter->letter . ' : ' . $letter->value);
                    }
                    break;
            }

            $letter->nbIncrement++;
        }
    }

    if (!empty($unsetLetterIndex)) {
        unset($lettersValue[$unsetLetterIndex]);
    }

    if (!empty($addLetter)) {
        $lettersValue[$addLetter->letter] = $addLetter;
    }

}
var_dump($lettersValue);
echo $nbIncrease;

class Letter
{

    const MAX_VAL_LETTER = 122;
    const MIN_VAL_LETTER = 97;

    public function __construct(
        public string $letter,
        public int $value = 0,
        public int $nbIncrement = 2
    ) {
    }

    public function getPrevLetter()
    {
        $number = ord($this->letter) - 1;

        if ($number < self::MIN_VAL_LETTER) {
            $number = self::MAX_VAL_LETTER;
        }

        return chr(
            $number
        );
    }

    function getNextLetter()
    {
        $number = ord($this->letter) + 1;

        if ($number > self::MAX_VAL_LETTER) {
            $number = self::MIN_VAL_LETTER;
        }

        return chr(
            $number
        );
    }
}
