<?php 

require_once(__DIR__ . '/data.php');

$nbIncrease = 0;
$lastNumber = null;

// Pour chaque nombre
foreach($data as $number) {

    // Si il y a un nombre précédent est qu'il est plus petit que le nombre actuelle
    // On incrémente le nombre de chiffre précédent < chiffre suivant
    if(!is_null($lastNumber) && $lastNumber < $number){
        $nbIncrease++;
    }

    // Le chiffre actuel devient le chiffre précédent
    $lastNumber = $number;

}

echo $nbIncrease;