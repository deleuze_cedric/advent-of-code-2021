<?php


class Cave
{

    const START = 'start';
    const END = 'end';

    public bool $isBigCave = false;
    public bool $isStart = false;
    public bool $isEnd = false;

    /**
     * @var array|CaveLink[]
     */
    protected array $links = [];

    public function __construct(
        public string $letters
    )
    {
        switch ($this->letters) {
            case self::END:
                $this->isEnd = true;
                break;
            case self::START:
                $this->isStart = true;
                break;
            default:
                $this->isBigCave = ctype_upper($this->letters);
                break;
        }
    }

    /**
     * @return array|CaveLink[]
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    protected function addLink(CaveLink $caveLink)
    {
        $this->links[] = $caveLink;
    }

    /**
     * @param Cave $cave
     * @return CaveLink
     */
    public function addCave(Cave $cave): CaveLink
    {
        foreach ($this->links as $link) {
            if (
                $link->cave1->letters === $cave->letters ||
                $link->cave2->letters === $cave->letters
            ) {
                $caveLink = $link;
                break;
            }
        }

        if (empty($caveLink)) {
            $caveLink = new CaveLink($this, $cave);
            $this->links[] = $caveLink;
            $cave->addLink($caveLink);
        }

        return $caveLink;
    }

}

class CaveLink
{

    public function __construct(
        public Cave $cave1,
        public Cave $cave2,
    )
    {
    }

    public function getTargetCave(Cave $sourceCave): Cave
    {
        return match ($sourceCave->letters) {
            $this->cave1->letters => $this->cave2,
            default => $this->cave1,
        };
    }

}

class Cavern
{

    /**
     * @var array|Cave[]
     */
    protected array $caves = [];

    /**
     * @return array|Cave[]
     */
    public function getCaves(): array
    {
        return $this->caves;
    }

    /**
     * @param string $letter
     * @return Cave
     */
    public function addCave(string $letter): Cave
    {
        if (!isset($this->caves[$letter])) {
            $cave = new Cave($letter);
            $this->caves[$letter] = $cave;
        } else {
            $cave = $this->caves[$letter];
        }

        return $cave;
    }

    public function findPaths(int $nbVisitSmallCave = 1): array
    {
        $paths = [];

        if (!empty($this->caves[Cave::START])) {
            $start = $this->caves[Cave::START];
            $paths = $this->getAllDiscoverableCavesFromCave($start, $nbVisitSmallCave);
        }

        return $paths;

    }

    public function getAllDiscoverableCavesFromCave(Cave $cave, int $nbVisitFirstSmallCave, array $previousCavesVisited = [], bool $smallCaveHasAlreadyBeenVistited = false): array
    {

        $paths = [];
        $previousCavesVisited[] = $cave->letters;

        if ($cave->letters !== Cave::END) {

            foreach ($cave->getLinks() as $link) {
                $targetCave = $link->getTargetCave($cave);

                $nbVisitCave = count(array_keys($previousCavesVisited, $targetCave->letters));

//                if (str_contains(implode(',', $previousCavesVisited), 'start,A,b,A,b')) {
//                    var_dump(implode(',', $previousCavesVisited), $targetCave->letters, $nbVisitCave, $smallCaveHasAlreadyBeenVistited);
//                }

                if ($targetCave->isEnd) {
                    $paths[] = implode(',', $previousCavesVisited);
                } else if (
                    !$targetCave->isStart &&
                    (
                        $targetCave->isBigCave ||
                        (
                            (
                                !$smallCaveHasAlreadyBeenVistited &&
                                $nbVisitCave < $nbVisitFirstSmallCave
                            ) || (
                                $nbVisitCave < 1
                            )
                        )
                    )
                ) {
                    $pathsFound = $this->getAllDiscoverableCavesFromCave(
                        $targetCave,
                        $nbVisitFirstSmallCave,
                        $previousCavesVisited,
                        $smallCaveHasAlreadyBeenVistited || (!$targetCave->isBigCave && $nbVisitCave + 1 >= $nbVisitFirstSmallCave)
                    );
                    foreach ($pathsFound as $path) {
                        $paths[] = $path;
                    }
                }

            }

        }

        return $paths;
    }

}

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');
$cavern = new Cavern();

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {
        $caves = explode('-', $line);
        $cavesAdded = [];

        foreach ($caves as $cave) {
            $newCave = $cavern->addCave($cave);

            foreach ($cavesAdded as $caveAddedBefore) {
                $newCave->addCave($caveAddedBefore);
            }

            $cavesAdded[] = $newCave;
        }


    }

    $file->next();
}
