<?php

require_once __DIR__ . '/Cave.php';


if (!empty($cavern)) {
    $paths = $cavern->findPaths();

    echo "Nb Possibilités : " . count($paths) . "<br/>";

    foreach ($paths as $path) {
        echo $path . "<br/>";
    }
}