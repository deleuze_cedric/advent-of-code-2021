<?php


class Page
{

    /**
     * @var array|Instruction[]
     */
    public array $instructions = [];

    /**
     * @var array[]Point[]
     */
    protected array $points = [];

    protected int $nbRow = 0;

    protected int $nbCol = 0;

    /**
     * @return int
     */
    public function getNbRow(): int
    {
        return $this->nbRow;
    }

    /**
     * @return int
     */
    public function getNbCol(): int
    {
        return $this->nbCol;
    }

    public function addPoint(int $row, int $col)
    {
        if ($row > $this->nbRow) {
            $this->nbRow = $row;
        }

        if ($col > $this->nbCol) {
            $this->nbCol = $col;
        }

        $this->points[$row][$col] = new Point($row, $col);
    }

    public function followInstructions(): array
    {
        /**
         * @var array|Point[] $points
         */
        $points = $this->points;
        $nbRow = $this->nbRow;
        $nbCol = $this->nbCol;

        foreach ($this->instructions as $index => $instruction) {

            $unsetPoints = [];

            foreach ($points as $row => $initialPoints) {
                foreach ($initialPoints as $col => $point) {

                    if (
                        (
                            $instruction->axe == Instruction::Y_AXE &&
                            $point->row > $instruction->value
                        ) ||
                        (
                            $instruction->axe == Instruction::X_AXE &&
                            $point->col > $instruction->value
                        )
                    ) {
                        $newRow = $row;
                        $newCol = $col;

                        switch ($instruction->axe) {
                            case Instruction::X_AXE:
//                    Colonnes
                                $newCol = $instruction->value - ($col - $instruction->value);
                                break;
                            case Instruction::Y_AXE:
//                    Lignes
                                $newRow = $instruction->value - ($row - $instruction->value);
                                break;
                        }

//                        echo "<pre>";
//                        var_dump($point->row . ' => ' . $newRow);
//                        var_dump($point->col . ' => ' . $newCol);
//                        echo "</pre>";

                        if ($newRow > -1 && $newCol > -1 && !isset($points[$newRow][$newCol])) {
                            $points[$newRow][$newCol] = new Point($newRow, $newCol);
                        }

                        $unsetPoints[] = [$row, $col];
                    } else if (
                        (
                            $instruction->axe == Instruction::Y_AXE &&
                            $point->row == $instruction->value
                        ) ||
                        (
                            $instruction->axe == Instruction::X_AXE &&
                            $point->col == $instruction->value
                        )
                    ) {
                        $unsetPoints[$point->row] = $point->col;
                    }
                }
            }

            switch ($instruction->axe) {
                case Instruction::X_AXE:
//                    Colonnes
                    $nbCol -= ($instruction->value);
                    break;
                case Instruction::Y_AXE:
//                    Lignes
                    $nbRow -= ($instruction->value);
                    break;
            }

            foreach ($unsetPoints as $rowCol) {
                unset($points[$rowCol[0]][$rowCol[1]]);
            }

//            var_dump($points);

            echo "<b>Etape " . ($index + 1) . " : " . (count($points, COUNT_RECURSIVE) - count($points)) . "<br/>";

//            echo $instruction . "<br/>";
//            echo $this->getHTMLForPoints($points, $nbRow, $nbCol);
        }

        return [
            $points,
            $nbRow,
            $nbCol
        ];
    }

    public
    function __toString(): string
    {
        return $this->getHTMLForPoints($this->points, $this->nbRow, $this->nbCol, $this->instructions);
    }

    public
    function getHTMLForPoints(array $points, int $nbRow, int $nbCol, array $instructions = []): string
    {
        $html = '<table>';

        $html .= '<tbody>';
        for ($row = 0; $row <= $nbRow; $row++) {
            $html .= '<tr>';
            for ($col = 0; $col <= $nbCol; $col++) {
                $html .= '<td>';
                if (!empty($points[$row][$col])) {
                    $html .= '#';
                } else {
                    $html .= '.';
                }
                $html .= '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</tbody>';

        $html .= '<tfooter>';
        foreach ($instructions as $instruction) {
            $html .= '<tr><td colspan="100%">' . $instruction . '</td></tr>';
        }
        $html .= '<tr><th colspan="100%">Total de : ' . (count($points, COUNT_RECURSIVE) - count($points)) . '</th></tr>';
        $html .= '</tfooter>';

        return $html . '</table>' . "<hr>";
    }
}

class Instruction
{

    const X_AXE = 'x';
    const Y_AXE = 'y';

    public function __construct(
        public string $axe,
        public int $value
    )
    {
    }

    public function __toString(): string
    {
        return 'fold along ' . $this->axe . '=' . $this->value;
    }
}

class Point
{

    public function __construct(
        public int $row,
        public int $col,
    )
    {
    }

}

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');
$page = new Page();
$isInstructionPart = false;

echo "<pre>";
while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {
        if ($isInstructionPart) {
            $instruction = str_replace("fold along ", '', $line);
            $instructionArray = explode('=', $instruction);
            $page->instructions[] = new Instruction($instructionArray[0], $instructionArray[1]);
        } else {
            $point = explode(',', $line);
            $page->addPoint($point[1], $point[0]);
        }
    } else {
        $isInstructionPart = true;
    }

    $file->next();
}

echo "</pre>";

//echo $page;

$res = $page->followInstructions();

echo $page->getHTMLForPoints($res[0], $res[1], $res[2]);