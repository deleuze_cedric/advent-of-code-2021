<?php

require_once __DIR__ . '/Octopus.php';

if (!empty($grid)) {

    $totalShine = 0;

    echo "<h3>Initial config :</h3>";
    echo $grid;

    for ($i = 1; $i <= 100; $i++) {
        $totalShine += $grid->newStep();

        echo "<h3>After step $i :</h3>";
        echo $grid;

    }


    echo "<br/><b>Total : </b>" . $totalShine;
}