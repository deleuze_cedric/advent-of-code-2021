<?php


require_once __DIR__ . '/Octopus.php';

if (!empty($grid)) {

    $i = 0;
    $nbOctopuses = count($grid->octopuses, COUNT_RECURSIVE) - count($grid->octopuses);

    echo "<h3>Initial config :</h3>";
    echo $grid;

    do {

        $i++;

        $nbShine = $grid->newStep();

        echo "<h3>After step $i :</h3>";
        echo $grid;

    } while ($nbShine != $nbOctopuses);


    echo "<br/><b>Step : </b>" . $i - 1;
}