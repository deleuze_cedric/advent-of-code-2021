<?php

use JetBrains\PhpStorm\Pure;

class Grid
{

    /**
     * @var array[]Octopus[]
     */
    public array $octopuses = [];

    /**
     * Retourne le nombre d'octopus qui ont brillé à cette étape
     * @return int
     */
    public function newStep(): int
    {
        $nbShine = 0;

        foreach ($this->octopuses as $row => $octopuses) {
            foreach ($octopuses as $col => $octopus) {
                $this->increaseOctopus($octopus);
            }
        }

        foreach ($this->octopuses as $row => $octopuses) {
            foreach ($octopuses as $col => $octopus) {
                if ($octopus->isShining()) {
                    $nbShine++;
                }
                $octopus->endStep();
            }
        }

        return $nbShine;
    }

    public function increaseOctopus(Octopus $currentOctopus)
    {
//        Si il ne brille pas et que son énergie après augmentation passe à plus de 9
        if (!$currentOctopus->isShining() && $currentOctopus->increaseEnergy()) {
            $xAdjacents = $currentOctopus->getXAdjacents();
            $yAdjacents = $currentOctopus->getYAdjacents();

            foreach ($this->octopuses as $row => $octopuses) {
                foreach ($octopuses as $col => $octopus) {
//                    Tous les octopus autour de lui vont avoir leur énergie d'augmenter
                    if (
                        !$currentOctopus->isEqualsTo($octopus) &&
                        in_array($octopus->x, $xAdjacents) &&
                        in_array($octopus->y, $yAdjacents)
                    ) {
                        $this->increaseOctopus($octopus);
                    }
                }
            }
        }
    }

    #[Pure] public function __toString(): string
    {
        $html = "<table>";

        foreach ($this->octopuses as $row => $octopuses) {
            $html .= "<tr>";
            foreach ($octopuses as $col => $octopus) {
                $html .= "<td>" . $octopus . "</td>";
            }
            $html .= "</tr>";
        }

        return $html . "</table>";
    }
}

class Octopus
{

    public function __construct(
        public int $energy,
        public int $x,
        public int $y,
    )
    {
    }

    public function getYAdjacents(): array
    {

        return [
            $this->y - 1,
            $this->y,
            $this->y + 1,
        ];

    }

    public function getXAdjacents(): array
    {
        return [
            $this->x - 1,
            $this->x,
            $this->x + 1,
        ];
    }

    public function isShining(): bool
    {
        return $this->energy > 9;
    }

    /**
     * Retourne vrai si le poulple brille après avoir augmenté son énergie
     * @return bool
     */
    public function increaseEnergy(): bool
    {
        $this->energy++;
        if ($this->isShining()) {
            return true;
        }
        return false;
    }

    public function endStep()
    {
        if ($this->isShining()) {
            $this->energy = 0;
        }
    }

    public function isEqualsTo(Octopus $octopus): bool
    {
        return $this->x == $octopus->x &&
            $this->y == $octopus->y;
    }

    #[Pure] public function __toString(): string
    {
        return ($this->energy == 0 ? "<b style='color: blue;'>" : "") . $this->energy . ($this->energy == 0 ? "</b>" : "");
    }
}

$file = new SplFileObject(__DIR__ . '/data-test.txt', 'r');
$grid = new Grid();
$row = 0;

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {
        $octopuses = [];
        $energies = str_split($line);
        foreach ($energies as $index => $energy) {
            $octopuses[] = new Octopus($energy, $index, $row);
        }
        $grid->octopuses[$row] = $octopuses;
    }

    $row++;
    $file->next();
}
