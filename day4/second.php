<?php

class Board
{

    public array $lines = [];

    public function __toString()
    {
        $html = "<table>";

        foreach ($this->lines as $line) {

            $html .= "<tr>";

            foreach ($line->cells as $cell) {
                $html .= '<td>' . ($cell->checked ? '<b style="text-decoration: underline;">' : '') . $cell->number . ($cell->checked ? '</b>' : '') . '<td>';
            }

            $html .= "</tr>";
        }
        $html .= "</tr>";

        $html .= "</table>";

        return $html;
    }

    public function checkPresenceOfNumber(int $number)
    {
        foreach ($this->lines as $line) {
            $line->checkPresenceOfNumber($number);
        }
    }

    public function hasOneLineOrColumnFull()
    {
        $columns = [];

        foreach ($this->lines as $line) {
            if ($line->full()) {
                return true;
            }

            foreach ($line->cells as $index => $cell) {
                
                if (!isset($columns[$index])) {
                    $columns[$index] = 0;
                }

                if($cell->checked){
                    $columns[$index]++;
                }

                if($columns[$index] == count($line->cells)){
                    return true;
                }
            }
        }
        return false;
    }

    public function countNumberNotChecked()
    {
        $value = 0;

        foreach ($this->lines as $line) {
            // echo "Line : " . $line->countNumberNotChecked() . "<br/>";
            $value += $line->countNumberNotChecked();
        }

        return $value;
    }
}

class Line
{
    public array $cells;

    public function full()
    {
        $nbChecked = 0;

        foreach ($this->cells as $cell) {
            if ($cell->checked) {
                $nbChecked++;
            }
        }

        return $nbChecked == count($this->cells);
    }

    public function checkPresenceOfNumber(int $number)
    {
        foreach ($this->cells as $cell) {
            $cell->checked = $cell->checked || $cell->number == $number;
        }
    }

    public function countNumberNotChecked()
    {
        $value = 0;

        foreach ($this->cells as $cell) {
            if (!$cell->checked) {
                $value += (int)$cell->number;
            }
        }

        return $value;
    }
}

class Cell
{

    public function __construct(
        public int $number,
        public int $checked
    ) {
    }
}

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');

$firstLine = true;
$numbersCall = null;
$newBoard = false;
$boards = [];

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {

        if ($firstLine) {
            $numbers = explode(',', $line);
            $firstLine = false;
        } else {

            if ($newBoard) {
                $boards[] = new Board();
                $newBoard = false;
            }

            $lastIndexBoard = count($boards);
            $lastBoard = $boards[$lastIndexBoard - 1];

            $rowNumbers = preg_split('/( +)/', $line);
            // var_dump(preg_split('/(\d+?)(?= *)/', $line));

            $line = new Line();

            foreach ($rowNumbers as $index => $number) {
                $cell = new Cell($number, false);
                $line->cells[] = $cell;
            }

            $lastBoard->lines[] = $line;
        }
    } else {
        $newBoard = true;
    }

    $file->next();
}

$lastNumber = null;
$lastBoardFull = null;
$winBoards = [];

foreach ($numbers as $number) {

    // echo "<h2>" . $number . "</h2>";

    foreach ($boards as $indexBoard => $board) {

        $board->checkPresenceOfNumber($number);

        if ($board->hasOneLineOrColumnFull()) {
            $winBoards[$indexBoard] = $board;

            if (count($winBoards) == count($boards)) {
                $lastBoardFull = $board;
                break;
            }
        }

        // echo "<b>" . $indexBoard . ($board->hasOneLineOrColumnFull() ? " => Win" : "") . "</b>";
        // echo $board . "<br/>";
    }

    // echo "<hr>";


    if (!empty($lastBoardFull)) {
        $lastNumber = $number;
        break;
    }
}

if (!empty($lastNumber) && !empty($lastBoardFull)) {
    // echo $lastBoardFull->countNumberNotChecked() . "<br/>";
    // echo $lastNumber . "<br/>";
    echo $lastBoardFull->countNumberNotChecked() * $lastNumber;
} else {
    echo "Erreur dans les calculs";
}
