<?php

class Board
{

    public array $lines = [];

    public function __toString()
    {
        $html = "<table>";

        foreach ($this->lines as $line) {

            $html .= "<tr>";

            foreach ($line->cells as $cell) {
                $html .= '<td>' . ($cell->checked ? '<b>' : '') . $cell->number . ($cell->checked ? '</b>' : '') . '<td>';
            }

            $html .= "</tr>";
        }
        $html .= "</tr>";

        $html .= "</table>";

        return $html;
    }

    public function checkPresenceOfNumber(int $number)
    {
        foreach ($this->lines as $line) {
            $line->checkPresenceOfNumber($number);
        }
    }

    public function hasOneLineOrColumnFull()
    {
        $columns = [];

        foreach ($this->lines as $line) {
            if ($line->full()) {
                return true;
            }

            foreach ($line->cells as $index => $cell) {
                
                if (!isset($columns[$index])) {
                    $columns[$index] = 0;
                }

                if($cell->checked){
                    $columns[$index]++;
                }

                if($columns[$index] == count($line->cells)){
                    return true;
                }
            }
        }
        return false;
    }

    public function countNumberNotChecked(){
        $value = 0;

        foreach($this->lines as $line){
            $value += $line->countNumberNotChecked();
        }

        return $value;
    }
}

class Line
{
    public array $cells;

    public function full()
    {
        $nbChecked = 0;

        foreach ($this->cells as $cell) {
            if ($cell->checked) {
                $nbChecked++;
            }
        }

        return $nbChecked == count($this->cells);
    }

    public function checkPresenceOfNumber(int $number)
    {
        foreach ($this->cells as $cell) {
            $cell->checked = $cell->checked || $cell->number == $number;
        }
    }

    public function countNumberNotChecked(){
        $value = 0;

        foreach ($this->cells as $cell) {
            if(!$cell->checked){
                $value += (int)$cell->number;
            }
        }

        return $value;
    }
}

class Cell
{

    public function __construct(
        public int $number,
        public int $checked
    ) {
    }
}

$file = new SplFileObject(__DIR__ . '/data.txt', 'r');

$firstLine = true;
$numbersCall = null;
$newBoard = false;
$boards = [];

while (!$file->eof()) {
    $line = $file->current();
    $line = trim($line);

    if (!empty($line)) {

        if ($firstLine) {
            $numbers = explode(',', $line);
            $firstLine = false;
        } else {

            if ($newBoard) {
                $boards[] = new Board();
                $newBoard = false;
            }

            $lastIndexBoard = count($boards);
            $lastBoard = $boards[$lastIndexBoard - 1];

            $rowNumbers = preg_split('/( +)/', $line);
            // var_dump(preg_split('/(\d+?)(?= *)/', $line));

            $line = new Line();

            foreach ($rowNumbers as $index => $number) {
                $cell = new Cell($number, false);
                $line->cells[] = $cell;
            }

            $lastBoard->lines[] = $line;
        }
    } else {
        $newBoard = true;
    }

    $file->next();
}

$lastNumber = null;
$boardFull = null;

foreach ($numbers as $number) {

    $lineFull = false;

    foreach ($boards as $board) {

        $board->checkPresenceOfNumber($number);

        if ($board->hasOneLineOrColumnFull()) {
            $lineFull = true;
            $boardFull = $board;
            break;
        }

        // echo $board . "<br/>";

    }


    if ($lineFull) {
        $lastNumber = $number;
        break;
    }
}

if (!empty($lastNumber) && !empty($boardFull)) {
    echo $boardFull->countNumberNotChecked() * $lastNumber;
} else {
    echo "Erreur dans les calculs";
}
